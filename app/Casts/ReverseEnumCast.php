<?php

namespace App\Casts;

use BackedEnum;
use Spatie\LaravelData\Casts\EnumCast;
use Spatie\LaravelData\Casts\Uncastable;
use Spatie\LaravelData\Exceptions\CannotCastEnum;
use Spatie\LaravelData\Support\Creation\CreationContext;
use Spatie\LaravelData\Support\DataProperty;
use Throwable;

class ReverseEnumCast extends EnumCast
{
    public function cast(DataProperty $property, mixed $value, array $properties, CreationContext $context): BackedEnum|Uncastable
    {
        $type = $this->type ?? $property->type->findAcceptedTypeForBaseType(BackedEnum::class);

        if ($type === null) {
            return Uncastable::create();
        }

        try {
            return $type::reverseFrom($value);
        } catch (Throwable $e) {
            throw CannotCastEnum::create($type, $value);
        }
    }
}
