<?php

namespace App\Data;

use Illuminate\Support\Arr;
use Spatie\LaravelData\Data;
use Spatie\LaravelData\Support\Transformation\TransformationContext;
use Spatie\LaravelData\Support\Transformation\TransformationContextFactory;

class BaseData extends Data
{
    public function transform(
        null|TransformationContextFactory|TransformationContext $transformationContext = null,
    ): array {
        $payload = parent::transform($transformationContext);

        $dottedArray = [];

        foreach ($payload as $key => $value) {
            Arr::set($dottedArray, $key, $value);
        }

        return $dottedArray;
    }
}
