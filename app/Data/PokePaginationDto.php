<?php

namespace App\Data;

class PokePaginationDto
{
    public function __construct(
        public int $limit,
        public int $offset,
    ) {
    }

    public function toArray(): array
    {
        return (array) $this;
    }
}
