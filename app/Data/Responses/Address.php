<?php

namespace App\Data\Responses;

use App\Data\BaseData;

class Address extends BaseData
{
    public string $address;
    public string $city;
    public string $postalCode;
    public ?string $state;
}
