<?php

namespace App\Data\Responses;

use App\Data\BaseData;

class Dimensions extends BaseData
{
    public ?float $weight;
    public ?float $height;
}
