<?php

namespace App\Data\Responses;

use App\Enums\BackedHairColor;
use App\Enums\BloodType;
use Spatie\LaravelData\Attributes\MapInputName;
use Spatie\LaravelData\Attributes\MapOutputName;
use Spatie\LaravelData\Attributes\Validation\Email;

class UserResponse extends BaseResponse
{
    public int $id;
    public string $firstName;
    public string $lastName;
    #[MapInputName('lastName')]
    public string $surname;
    #[MapInputName('lastName')]
    public string $name;

    #[Email]
    public string $email;
    #[MapInputName('hair.color')]
    public string $hairColor;

    #[MapInputName('hairColor')]
    public BackedHairColor $enumHairColor;

    #[MapInputName('address')]
    public Address $address;

    #[MapInputName('address.address')]
    public string $addressString;

    #[MapOutputName('university.name')]
    public string $university;

    public BloodType $bloodGroup;

    public float $weight;
    public float $height;

    public Dimensions $dimensions;

    public static function prepareForPipeline(array $properties): array
    {
        $properties['dimensions'] = [
            'weight' => $properties['weight'] ?? null,
            'height' => $properties['height'] ?? null,
        ];

        return $properties;
    }

    public function with(): array
    {
        return [
            'withDimensions' => [
                'weight' => $this->weight ?? null,
                'eight' => $this->height ?? null,
            ]
        ];
    }
}
