<?php

namespace App\Data\Responses;

use Spatie\LaravelData\DataCollection;

class UsersPipelineResponse extends BaseResponse
{
    /** @var UserResponse[] */
    public DataCollection $users;

    public static function prepareForPipeline(array $properties): array
    {
        return [
            'users' => $properties,
        ];
    }
}
