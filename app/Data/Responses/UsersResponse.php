<?php

namespace App\Data\Responses;

use Spatie\LaravelData\DataCollection;

class UsersResponse extends BaseResponse
{
    /** @var UserResponse[] $users */
    public DataCollection $users;
}
