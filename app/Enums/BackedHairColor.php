<?php

namespace App\Enums;

use App\Interfaces\ReverseEnumInterface;
use App\Traits\ReverseEnumTrait;

enum BackedHairColor: string implements ReverseEnumInterface
{
    use ReverseEnumTrait;

    case Black = 'BK-00';
    case Blond = 'BL-60';
    case Brown = 'BR-80';
    case Chestnut = 'CH-120';
    case Auburn = 'AU-92';
}
