<?php

namespace App\Enums;

enum BloodType: string
{
    case ANegative = 'A-';
    case OPositive = 'O+';
    case BPositive = 'B+';
    case APositive = 'A+';
    case BNegative = 'B-';
    case ONegative = 'O-';
    case ABNegative = 'AB-';
}
