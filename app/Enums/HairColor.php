<?php

namespace App\Enums;

enum HairColor
{
    case Black;
    case Blond;
    case Brown;
    case Chestnut;
    case Auburn;
}
