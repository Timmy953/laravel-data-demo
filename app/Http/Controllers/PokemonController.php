<?php

namespace App\Http\Controllers;

use App\Data\PokePaginationDto;
use App\Services\PokeService;
use Illuminate\Http\Request;

class PokemonController extends Controller
{
    public function __construct(protected PokeService $pokeService)
    {
    }

    public function index(Request $request)
    {
        $limit = $request->get('page_size', 20);

        $pagination = new PokePaginationDto(
            limit: $limit,
            offset: (($request->get('page') ?? 1) - 1) * $limit,
        );

        $pokemon = $this->pokeService->getPokemon($pagination);

        dd($pokemon);
    }

    public function indexWithExplicitFields(Request $request)
    {
        $limit = $request->get('page_size', 20);

        $pagination = new PokePaginationDto(
            limit: $limit,
            offset: (($request->get('page') ?? 1) - 1) * $limit,
        );

        $pokemon = $this->pokeService->getPokemonExplicitFields($pagination);

        dd($pokemon);
    }
}
