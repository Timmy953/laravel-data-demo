<?php

namespace App\Http\Controllers;

use App\Services\DummyService;

class UserController extends Controller
{
    public function __construct(protected DummyService $dummyService)
    {
    }

    public function user()
    {
        $user = $this->dummyService->user();

        dd($user->transform());
    }

    public function index()
    {
        $users = $this->dummyService->users();

        dd($users->transform(), $users->users->transform());
    }

    public function indexArray()
    {
        $users = $this->dummyService->usersArray();

        dd($users);
    }

    public function indexArrayWithPipeline()
    {
        $users = $this->dummyService->usersAsArrayWithPipeline();

        dd($users->transform(), $users->users->transform());
    }
}
