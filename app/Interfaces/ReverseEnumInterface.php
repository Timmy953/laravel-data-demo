<?php

namespace App\Interfaces;

interface ReverseEnumInterface
{
    public static function reverseFrom(string $enum): ?self;
}
