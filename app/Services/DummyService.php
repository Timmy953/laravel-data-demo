<?php

namespace App\Services;

use App\Data\Responses\BaseResponse;
use App\Data\Responses\UserResponse;
use App\Data\Responses\UsersPipelineResponse;
use App\Data\Responses\UsersResponse;

class DummyService
{
    public function __construct(
        protected JsonClient $json,
    ) {
    }

    public function user(): BaseResponse|UserResponse
    {
        return $this->json->request(
            'user.json',
            UserResponse::class,
        );
    }

    public function users(): BaseResponse|UsersResponse
    {
        return $this->json->request(
            'users-as-object.json',
            UsersResponse::class,
        );
    }

    public function usersArray(): array
    {
        return $this->json->request(
            'users-as-array.json',
            UserResponse::class,
            collection: true,
        );
    }

    public function usersAsArrayWithPipeline(): BaseResponse|UsersPipelineResponse
    {
        return $this->json->request(
            'users-as-array.json',
            UsersPipelineResponse::class,
        );
    }
}
