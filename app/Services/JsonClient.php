<?php

namespace App\Services;

use App\Data\Responses\BaseResponse;
use Illuminate\Support\Facades\Storage;
use RuntimeException;

class JsonClient
{
    public function request(string $path, string $className, array $data = [], bool $collection = false): BaseResponse|array
    {
        if (!is_subclass_of($className, BaseResponse::class)) {
            throw new RuntimeException('Class ' . $className . ' should exist and extend ' . BaseResponse::class);
        }

        $content = Storage::get($path);

        if (!$collection) {
            return $className::from($content);
        }

        return $className::collect(json_decode($content));
    }
}
