<?php

namespace App\Services;

use App\Data\PokePaginationDto;
use Illuminate\Support\Facades\Http;

class PokeService
{
    protected string $url = 'https://pokeapi.co/api/v2/pokemon/';

    public function getPokemon(PokePaginationDto $paginationDto)
    {
        $response = Http::get($this->url, $paginationDto->toArray())->body();

        return json_decode($response);
    }

    public function getPokemonExplicitFields(PokePaginationDto $paginationDto)
    {
        $response = Http::get($this->url, [
            'limit' => $paginationDto->limit,
            'offset' => $paginationDto->offset,
        ])->body();

        return json_decode($response);
    }
}
