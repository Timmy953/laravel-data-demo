<?php

namespace App\Traits;

trait ReverseEnumTrait
{
    public static function names(): array
    {
        return array_column(static::cases(), 'name');
    }

    public static function reverseFrom(string $enum): ?self
    {
        if (in_array($enum, static::names(), true)) {
            return constant("static::$enum");
        }

        return null;
    }
}
