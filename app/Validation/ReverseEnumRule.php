<?php

namespace App\Validation;

use Illuminate\Validation\Rules\Enum;
use TypeError;

class ReverseEnumRule extends Enum
{
    /**
     * Determine if the validation rule passes.
     *
     * @param  string  $attribute
     * @param  mixed  $value
     */
    public function passes($attribute, $value): bool
    {
        if ($value instanceof $this->type) {
            return true;
        }

        if (is_null($value) || !enum_exists($this->type) || !method_exists($this->type, 'reverseFrom')) {
            return false;
        }

        try {
            return !is_null($this->type::reverseFrom($value));
        } catch (TypeError) {
            return false;
        }
    }
}
