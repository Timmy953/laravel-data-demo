<?php

namespace App\Validation;

use App\Interfaces\ReverseEnumInterface;
use Spatie\LaravelData\Attributes\Validation\Enum;
use Spatie\LaravelData\RuleInferrers\RuleInferrer;
use Spatie\LaravelData\Support\DataProperty;
use Spatie\LaravelData\Support\Validation\PropertyRules;
use Spatie\LaravelData\Support\Validation\ValidationContext;

class ReverseEnumRuleInferrer implements RuleInferrer
{
    public function handle(DataProperty $property, PropertyRules $rules, ValidationContext $context): PropertyRules
    {
        if ($enumClass = $property->type->findAcceptedTypeForBaseType(ReverseEnumInterface::class)) {
            $rules->removeType(Enum::class);

            $rules->add(new ReverseEnumValidationAttribute($enumClass));
        }

        return $rules;
    }
}
