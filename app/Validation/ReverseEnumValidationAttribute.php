<?php

namespace App\Validation;

use Spatie\LaravelData\Attributes\Validation\ObjectValidationAttribute;
use Spatie\LaravelData\Support\Validation\References\RouteParameterReference;
use Spatie\LaravelData\Support\Validation\ValidationPath;

class ReverseEnumValidationAttribute extends ObjectValidationAttribute
{
    protected ReverseEnumRule $rule;

    public function __construct(string|ReverseEnumRule|RouteParameterReference $enum)
    {
        $this->rule = $enum instanceof ReverseEnumRule
            ? $enum
            : new ReverseEnumRule((string) $enum);
    }

    public static function keyword(): string
    {
        return 'reverse_enum';
    }

    public function getRule(ValidationPath $path): object|string
    {
        return $this->rule;
    }

    public static function create(string ...$parameters): static
    {
        return new self(new ReverseEnumRule($parameters[0]));
    }
}
