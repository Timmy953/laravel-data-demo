<?php

use App\Http\Controllers\PokemonController;
use App\Http\Controllers\UserController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "api" middleware group. Make something great!
|
*/

Route::get('/user', [UserController::class, 'user']);
Route::get('/users', [UserController::class, 'index']);
Route::get('/users-array', [UserController::class, 'indexArray']);
Route::get('/users-array-pipeline', [UserController::class, 'indexArrayWithPipeline']);

Route::get('/pokemon', [PokemonController::class, 'index']);
Route::get('/pokemon-2', [PokemonController::class, 'indexWithExplicitFields']);

